from django.urls import path

from . import views

app_name = 'old_profile_page'

urlpatterns = [
    path('', views.index, name='index'),
]