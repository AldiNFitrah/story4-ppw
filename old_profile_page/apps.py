from django.apps import AppConfig


class OldProfilePageConfig(AppConfig):
    name = 'old_profile_page'
