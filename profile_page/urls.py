from django.urls import path

from . import views

app_name = 'profile_page'

urlpatterns = [
    path('', views.index, name='index'),
    path('favicon.ico', views.index),
    path('mata-kuliah/', views.matkul, name='matkul'),
    path('mata-kuliah/hapus/<int:id>', views.matkul_hapus, name='matkul-hapus'),
    path('mata-kuliah/detail/<int:id>', views.matkul_detail, name='matkul-detail'),
    path('testimonials', views.testi, name='testi'),
    path('<str:gmt>', views.index_with_time, name='index_with_time'),
]