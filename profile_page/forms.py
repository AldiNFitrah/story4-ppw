from django import forms  
from .models import Matkul

class MatkulForm(forms.ModelForm) :
    class Meta :
        model = Matkul
        fields = '__all__'
        exclude = ['id']