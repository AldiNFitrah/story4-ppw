from django.db import models
from django import forms
import datetime

# Create your models here.
class Matkul(models.Model) :
    SEMESTER_CHOICE = [
        ("Gasal", "Gasal"),
        ("Genap", "Genap"),
    ]
    TA_CHOICE = [
        ("2019/2020", "2019/2020"),
        ("2020/2021", "2020/2021"),
    ]
    id = models.AutoField(primary_key=True)
    nama = models.CharField(max_length=32, blank=False)
    dosen = models.CharField(max_length=32, blank=False)
    sks = models.IntegerField(blank=False)
    deskripsi = models.TextField(blank=True)
    semester = models.CharField(max_length=32, choices=SEMESTER_CHOICE, default="Gasal")
    tahun_ajaran = models.CharField(max_length=32, choices=TA_CHOICE, default="2019/2020")
    ruang = models.CharField(max_length=16, blank=False)

    def __str__(self):
        return self.nama

class Task(models.Model) :
    nama = models.CharField(max_length=32)
    desc = models.TextField(blank=True)
    deadline = models.DateTimeField()
    matkul = models.ForeignKey(Matkul, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama

    def color(self):
        deadline = self.deadline
        now = datetime.datetime.now(datetime.timezone.utc)
        difference = (deadline - now).days
        color = "0000"
        if(difference < 0) : color = "#444444"
        else : color = "#" + hex(150 + difference*2)[2:] + color
        
        return color

    class Meta :
        ordering = ['deadline']