from django.shortcuts import render
from django.http import HttpResponse
from .forms import MatkulForm
from .models import *
from django.shortcuts import get_list_or_404, get_object_or_404, redirect
from django.urls import reverse
import datetime

def index(request) :
    now = datetime.datetime.now()
    time = {
        "now" : now.strftime("%H:%M")
    }
    return render(request, "profile_page/index.html", context=time)

def index_with_time(request, gmt) :
    gmt = int(gmt)
    now = datetime.datetime.now()
    hour = (now.hour + gmt) % 24
    now = now.replace(hour = hour)
    time = {
        "now" : now.strftime("%H:%M"),
    }
    return render(request, "profile_page/index.html", context=time)

def testi(request) :
    return render(request, "profile_page/testimonials.html")

def matkul(request) :
    if(request.method == "POST") :
        matkul_form = MatkulForm(request.POST)

        if(matkul_form.is_valid) :
            matkul_form.save()
        
        else :
            print(matkul_form.errors)

    matkul_form = MatkulForm()
    data = Matkul.objects.all()

    return render(request, "profile_page/matkul.html", {"form":matkul_form, "data":data})

def matkul_detail(request, id) :
    data = Matkul.objects.get(id=id)
    tasks = Task.objects.all().filter(matkul=data)

    return render(request, "profile_page/matkul-detail.html", {"item" : data, "tasks" : tasks})

def matkul_hapus(request, id) :
    tmp_obj = get_object_or_404(Matkul, id=id)
    try :
        tmp_obj.delete()
    except :
        pass
    
    return redirect("/mata-kuliah/")